package com.changgou.consumer.service.impl;

import com.changgou.consumer.service.SeckillOrderService;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.dao.SeckillOrderMapper;
import com.changgou.seckill.pojo.SeckillOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 戴金华
 * @date 2019-12-30 21:29
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {


    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    @Autowired
    private SeckillOrderMapper seckillOrderMapper;

    /**
     * 创建mysql的订单
     * @param seckillOrder
     * @return
     */
    @Override
    @Transactional
    public int createOrder(SeckillOrder seckillOrder) {

        int result = seckillGoodsMapper.updateSeckillGoodsStockCount(seckillOrder.getSeckillId());
        if (result<=0){
            return result;
        }
        result = seckillOrderMapper.insertSelective(seckillOrder);
        if (result<=0){
            return result;
        }
        return 1;
    }
}
