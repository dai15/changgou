package com.changgou.consumer.service;

import com.changgou.seckill.pojo.SeckillOrder;

/**
 * @author 戴金华
 * @date 2019-12-30 21:20
 */
public interface SeckillOrderService {

    /**
     * 创建mysql的订单
     * @param seckillOrder
     * @return
     */
    int createOrder(SeckillOrder seckillOrder);
}
