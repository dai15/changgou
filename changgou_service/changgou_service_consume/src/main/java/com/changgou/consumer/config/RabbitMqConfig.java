package com.changgou.consumer.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 戴金华
 * @date 2019-12-30 21:23
 */
@Configuration
public class RabbitMqConfig {

    public static final String SECKILL_ORDER_KEY = "seckill_order";

    @Bean
    public Queue getQueue(){
        return new Queue(SECKILL_ORDER_KEY,true);
    }
}
