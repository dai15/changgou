package com.changgou.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 戴金华
 * @date 2019-12-20 19:44
 */
@Configuration
public class RabbitMqConfig {
    //交换机名称
    public static final String GOODS_UP_EXCHANGE = "goods_up_exchange";
    public static final String GOODS_DOWN_EXCHANGE = "goods_down_exchange";

    //定义队列名称
    public static final String SEARCH_ADD_QUEUE = "search_add_queue";
    public static final String SEARCH_DELETE_QUEUE = "search_delete_queue";
    public static final String AD_UPDATE_QUEUE = "ad_update_queue";
    public static final String PAGE_CREATE_QUEUE = "page_create_queue";

    //声明队列
    @Bean(AD_UPDATE_QUEUE)
    public Queue ADD_UPDATE_QUEUE(){
        return new Queue(AD_UPDATE_QUEUE);
    }

    @Bean(SEARCH_ADD_QUEUE)
    public Queue SEARCH_ADD_QUEUE(){
        return new Queue(SEARCH_ADD_QUEUE);
    }

    @Bean(SEARCH_DELETE_QUEUE)
    public Queue SEARCH_DELETE_QUEUE(){
        return new Queue(SEARCH_DELETE_QUEUE);
    }


    @Bean(PAGE_CREATE_QUEUE)
    public Queue PAGE_CREATE_QUEUE(){
        return new Queue(PAGE_CREATE_QUEUE);
    }

    //声明交换机
    @Bean(GOODS_UP_EXCHANGE)
    public Exchange GOODS_UP_EXCHANGE(){
        return ExchangeBuilder.fanoutExchange(GOODS_UP_EXCHANGE).durable(true).build();
    }

    @Bean(GOODS_DOWN_EXCHANGE)
    public Exchange GOODS_DOWN_EXCHANGE(){
        return ExchangeBuilder.fanoutExchange(GOODS_DOWN_EXCHANGE).durable(true).build();
    }

    //队列绑定交换机
    @Bean
    public Binding AD_UPDATE_QUEUE_BINDING(@Qualifier(SEARCH_ADD_QUEUE) Queue queue,@Qualifier(GOODS_UP_EXCHANGE) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    @Bean
    public Binding SEARCH_DELETE_QUEUE_BINDING(@Qualifier(SEARCH_DELETE_QUEUE) Queue queue,@Qualifier(GOODS_DOWN_EXCHANGE) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    @Bean
    public Binding PAGE_CREATE_QUEUE_BINDINF(@Qualifier(PAGE_CREATE_QUEUE) Queue queue,@Qualifier(GOODS_UP_EXCHANGE) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }
}
