package com.changgou.util;

import com.alibaba.otter.canal.protocol.CanalEntry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * canal工具类
 * @author 戴金华
 * @date 2019-12-20 10:47
 */
public class CanalUtil {


    /**
     * 列集合转换为map
     * @param columnList
     * @return
     */
    public static Map<String,String> convertToMap(List<CanalEntry.Column> columnList){
        Map<String,String> map = new HashMap();
        columnList.forEach(c->map.put(c.getName(),c.getValue()));
        return map;
    }
}
