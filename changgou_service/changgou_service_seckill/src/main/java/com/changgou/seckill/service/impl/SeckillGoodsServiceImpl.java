package com.changgou.seckill.service.impl;

import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.seckill.service.SeckillGoodsService;
import com.changgou.seckill.task.PushGoodsToRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-28 20:59
 */
@Service
public class SeckillGoodsServiceImpl implements SeckillGoodsService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 根据时间段查询秒杀商品集合
     * @param time
     * @return
     */
    @Override
    public List<SeckillGoods> searchList(String time) {
        return redisTemplate.boundHashOps(PushGoodsToRedis.SECKILL_GOODS_KEY+time).values();
    }
}
