package com.changgou.seckill.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.seckill.service.SeckillGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-28 20:56
 */
@RestController
@RequestMapping("/seckill")
public class SeckillGoodsController {

    @Autowired
    private SeckillGoodsService seckillGoodsService;

    /**
     * 根据时间段查询秒杀商品的集合
     * @param time
     * @return
     */
    @GetMapping("/searchList")
    public Result<List<SeckillGoods>> searchList(@RequestParam("time") String time){
        List<SeckillGoods> seckillGoods = seckillGoodsService.searchList(time);
        return new Result<>(true,StatusCode.OK,"查询成功",seckillGoods);
    }



}
