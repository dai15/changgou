package com.changgou.seckill.controller;

import com.changgou.entity.Result;
import com.changgou.entity.SeckillStatus;
import com.changgou.entity.StatusCode;
import com.changgou.seckill.service.SeckillOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 戴金华
 * @date 2019-12-30 10:33
 */
@RestController
@RequestMapping("/seckillOrder")
public class SeckillOrderController {


    @Autowired
    private SeckillOrderService seckillOrderService;

    /**
     * 实现秒杀下单
     * @param time
     * @param id
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestParam("time") String time,@RequestParam("id") Long id){
        //用户名暂时写死
        String username = "heima";
        seckillOrderService.add(time,id,username);
        return new Result(true, StatusCode.OK,"秒杀订单下单成功");
    }

    @GetMapping("/query")
    public Result query(){
        String username = "heima";
        SeckillStatus seckillStatus = seckillOrderService.queryStatus(username);
        if (seckillStatus!=null){
            return new Result(true,StatusCode.OK,"查询成功",seckillStatus);
        }
        return new Result(false,StatusCode.OK,"下单失败");
    }
}
