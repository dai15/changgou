package com.changgou.seckill.service;


import com.changgou.entity.SeckillStatus;

/**
 * @author 戴金华
 * @date 2019-12-28 20:59
 */
public interface SeckillOrderService {

    /**
     * 实现秒杀下单
     * @param time
     * @param id
     * @return
     */
    void add(String time, Long id, String username);

    /**
     * 用户抢单状态查询
     * @param username
     */
    SeckillStatus queryStatus(String username);
}
