package com.changgou.seckill.service;


import com.changgou.seckill.pojo.SeckillGoods;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-28 20:59
 */
public interface SeckillGoodsService {


    /**
     * 根据时间段查询秒杀商品集合
     * @param time
     * @return
     */
    List<SeckillGoods> searchList(String time);
}
