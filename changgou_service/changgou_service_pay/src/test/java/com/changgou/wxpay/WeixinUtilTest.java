package com.changgou.wxpay;

import com.github.wxpay.sdk.WXPayUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-25 21:40
 */
public class WeixinUtilTest {

    @Test
    public void testDemo() throws Exception{
        //随机字符串
        String str = WXPayUtil.generateNonceStr();
        System.out.println(str);
        //将Map转换成成xml
        Map map = new HashMap();
        map.put("username","dai");
        map.put("id","123");
        map.put("password","dai");
        String xml = WXPayUtil.generateSignedXml(map, "daijinhua");
        System.out.println("将map转换成xml"+xml);

        //将xml转换成map
        Map<String, String> xmlToMap = WXPayUtil.xmlToMap(xml);
        System.out.println("转换成的map集合是"+xmlToMap);
    }
}
