package com.changgou.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author 戴金华
 * @date 2019-12-26 21:25
 */
@Configuration
public class MqConfig {

    @Autowired
    private Environment environment;


    //创建队列
    @Bean(name = "orderQueue")
    public Queue orderQueue(){
        return new Queue(environment.getProperty("mq.pay.queue.order"),true);
    }

    //创建交换机
    @Bean(name = "orderExchange")
    public Exchange orderExchange(){
        return new DirectExchange(environment.getProperty("mq.pay.exchange.order"));
    }

    //创建绑定策略
    @Bean
    public Binding orderExchangeQueue(@Qualifier("orderQueue") Queue queue,@Qualifier("orderExchange") Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(environment.getProperty("mq.pay.routing.key")).noargs();
    }
}
