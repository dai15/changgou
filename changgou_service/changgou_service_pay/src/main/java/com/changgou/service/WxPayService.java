package com.changgou.service;

import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-25 22:23
 */
public interface WxPayService {

    /**
     * 生成支付二维码
     * @param parameterMap
     * @return
     */
    Map nativePay(Map<String,String> parameterMap);

    /**
     * 查询订单状态
     * @param parameterMap
     * @return
     */
    Map queryOrderPay(Map<String,String> parameterMap);

    /**
     * 关闭微信支付订单
     * @param orderId
     * @return
     */
    Map closeOrder(String orderId);
}
