package com.changgou.order.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;


/**
 * @author 戴金华
 * @date 2019-12-17 15:06
 */
@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {


    @Autowired
    private CartService cartService;


    /**
     * 添加购物车
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/add")
    public Result add(@RequestParam("skuId") String skuId,@RequestParam("num") Integer num){
        String username = "heima";
        cartService.add(skuId,num,username);
        return new Result(true, StatusCode.OK,"添加购物车成功");
    }

    /***
     * 查询购物车的数据
     * @return
     */
    @GetMapping("/list")
    public List<Map<String, Object>> list(){
        List<Map<String, Object>> cartList = cartService.list("heima");
        return cartList;
    }

    /**
     * 更新选中的商品状态
     * @param skuId
     * @param checked
     * @return
     */
    @PostMapping("/updateChecked")
    public List<Map<String,Object>> updateChecked(String skuId,boolean checked){
        cartService.updateChecked("heima",skuId,checked);
        List<Map<String, Object>> cartList = cartService.list("heima");
        return cartList;
    }

    /**
     *  删除选中的商品
     * @return
     */
    @PostMapping("delCart")
    public Result delCart(){
        String username = "heima";
        cartService.delCart(username);
        List<Map<String, Object>> cartList = cartService.list(username);
        return new Result(true,StatusCode.OK,"删除成功",cartList);
    }
}
