package com.changgou.order.service;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-17 15:08
 */
public interface CartService {

    /**
     * 添加购物车
     * @param skuId   商品id
     * @param num     商品数量
     * @param username  用户名
     */
    void add(String skuId, Integer num, String username);


    /**
     * 查询购物车的数据
     * @param username
     * @return
     */
    List<Map<String,Object>> list(String username);

    /**
     * 更新勾选状态
     * @param username
     * @param skuId
     * @param checked
     */
    void updateChecked(String username,String skuId,boolean checked);

    /**
     * 删除勾选中的商品
     * @param username
     */
    void delCart(String username);
}
