package com.changgou.order.task;


import com.alibaba.fastjson.JSON;
import com.changgou.order.pojo.Order;
import com.changgou.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单的定时任务 自动收货
 * @author 戴金华
 * @date 2019-12-28 13:31
 */
@Component
public class OrderTask {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private OrderService orderService;

    /**
     * 每隔15天查询reids中缓存 如果有缓存 且其订单为已发货状态 将其收货状态置为已收货
     */
    @Scheduled(cron = "0 0 0 1/15 * ? ")
    public void autoConfirmOrder(){
        String orderJson = (String) redisTemplate.boundValueOps("SHIPED_ORDERED").get();
        List<Order> orders = JSON.parseArray(orderJson, Order.class);

        for (Order order : orders) {
            if ("1".equals(order.getConsignStatus())&&"2".equals(order.getOrderStatus())){
                //如果订单已发货 则自动收货
                orderService.autoConfirmOrder(order);
            }
        }
    }
}
