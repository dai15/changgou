package com.changgou.order.listener;

import com.changgou.order.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 延迟消息的监听类
 * @author 戴金华
 * @date 2019-12-28 15:33
 */
@Component
@RabbitListener(queues = "queue_ordertimeout")
public class OrderDelayMessageListener {

    @Autowired
    private OrderService orderService;

    /**
     * 延迟队列处理订单 关闭订单
     * @param message
     */
    @RabbitHandler
    public void getMessage(String message){
        System.out.println("接受到的消息是"+message);
        orderService.closeOrder(message);
    }
}
