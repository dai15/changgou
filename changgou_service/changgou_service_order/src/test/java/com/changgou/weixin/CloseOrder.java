package com.changgou.weixin;


import com.github.wxpay.sdk.WXPayUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试关闭订单
 * @author 戴金华
 * @date 2019-12-27 18:46
 */
public class CloseOrder {
    public static void main(String[] args) throws Exception {

        HttpClient client = HttpClients.createDefault();

        String url = "https://api.mch.weixin.qq.com/pay/closeorder";
        HttpPost httpPost = new HttpPost(url);

        //构建参数条件
        Map<String,String> map = new HashMap();
        map.put("appid","wx8397f8696b538317");
        map.put("mch_id","1473426802");
        map.put("out_trade_no","123456");
        map.put("nonce_str", WXPayUtil.generateNonceStr());

        String generateSignedXml = WXPayUtil.generateSignedXml(map, "itcast");
        System.out.println(generateSignedXml);

//        String xml = "<xml><appid>wx2421b1c4370ec43b</appid><mch_id>10000100</mch_id><nonce_str>4ca93f17ddf3443ceabf72f26d64fe0e</nonce_str><out_trade_no>1415983244</out_trade_no><sign>59FF1DF214B2D279A0EA7077C54DD95D</sign></xml>";
//
//        ArrayList<BasicNameValuePair> parametes = new ArrayList<>();
//        parametes.add(new BasicNameValuePair("xml",xml));
//        httpPost.setEntity(new UrlEncodedFormEntity(parametes,"UTF-8"));
//        HttpResponse response = client.execute(httpPost);
//
//        HttpEntity entity = response.getEntity();
//        String result = EntityUtils.toString(entity, "UTF-8");
//        System.out.println(result);
//        System.out.println(WXPayUtil.xmlToMap(result));


        HttpPost post = new HttpPost(url);
        List<BasicNameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("xml", generateSignedXml));
        post.setEntity(new UrlEncodedFormEntity(parameters,"UTF-8"));
        HttpResponse response = client.execute(post);
        System.out.println(response.toString());
        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity, "UTF-8");
        System.out.println(result);
    }
}
