package com.changgou.search.listener;

import com.changgou.search.service.SkuService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 戴金华
 * @date 2019-12-20 21:58
 */
@Component
public class SpuDeleteListener {

    @Autowired
    private SkuService skuService;

    @RabbitListener(queues = "search_delete_queue")
    public void searchDelete(String spuId){
        //接受spuId 删除索引库中的数据
        System.out.println("要删除的spu的id是:"+spuId);
        skuService.deleteDataById(spuId);
    }
}
