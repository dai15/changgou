package com.changgou.search.service;

import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-07 16:14
 */
public interface SkuService {

    /**
     * 将数据库数据导入到es中
     */
    void importData();


    /**
     * 根据id插入索引库
     * @param spuId
     */
    void importDataById(String spuId);

    /**
     * 商品搜索
     * @param searchMap
     * @return
     */
    Map<String,Object> search(Map<String,String> searchMap);


    /**
     * 根据spuId删除索引库的数据
     * @param id
     */
    void deleteDataById(String id);
}
