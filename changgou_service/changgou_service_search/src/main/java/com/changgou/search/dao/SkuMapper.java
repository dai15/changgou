package com.changgou.search.dao;

import com.changgou.search.pojo.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author 戴金华
 * @date 2019-12-07 16:15
 */
public interface SkuMapper extends ElasticsearchRepository<SkuInfo,Long> {
}
