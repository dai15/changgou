package com.changgou.search.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.search.service.SkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-07 16:28
 */
@RestController
@CrossOrigin
@RequestMapping("/search")
public class SkuController {

    @Autowired
    private SkuService skuService;


    /**
     * 将数据库数据导入到es中
     * @return
     */
    @GetMapping("/importData")
    public Result importData(){
        skuService.importData();
        return new Result();
    }

    /**
     * 查询
     * @param searchMap
     * @return
     */
    @GetMapping
    public Result search(@RequestParam(required = false) Map<String,String> searchMap){
        Map<String, Object> map = skuService.search(searchMap);
        return new Result(true, StatusCode.OK,"搜索成功",map);
    }
}
