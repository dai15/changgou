package com.changgou.search.listener;

import com.changgou.search.service.SkuService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 戴金华
 * @date 2019-12-20 20:03
 */
@Component
public class SpuListener {

    @Autowired
    private SkuService skuService;

    @RabbitListener(queues = "search_add_queue")
    public void searchAdd(String spuId){
        //接受队列里的消息 将数据导入到es中
        skuService.importDataById(spuId);
    }


}
