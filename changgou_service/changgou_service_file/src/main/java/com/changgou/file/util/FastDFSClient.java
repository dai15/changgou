package com.changgou.file.util;

import com.changgou.file.pojo.FastDFSFile;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * fastDFS信息的获取以及文件的相关操作
 * @author 戴金华
 * @date 2019-12-04 19:03
 */
public class FastDFSClient {

    private static Logger logger = LoggerFactory.getLogger(FastDFSClient.class);

    /**
     * 初始化加载FastDFS的TrackerServer的配置
     */
    static {
        try {
            String filePath = new ClassPathResource("fdfs_client.conf").getPath();
            ClientGlobal.init(filePath);
        }catch (Exception e){
            logger.error("fastDFS init error",e);
        }
    }

    /**
     * 文件上传
     * @param fastDFSFile
     * @return
     */
    public static String[] upload(FastDFSFile fastDFSFile) throws Exception {

        //创建一个Tracker客户端的访问对象TrackerClient
        TrackerClient trackerClient = new TrackerClient();

        //通过TrackerClient访问TrackerServer服务 获取连接
        TrackerServer connection = trackerClient.getConnection();

        //通过TrackerServer的连接信息可以获取Storage的连接信息 创建StorageClient对象存储Stroage信息
        StorageClient storageClient = new StorageClient(connection, null);

        //通过StorageClient访问Storage 实现文件上传 并获取文件上传之后的存储信息

        /**
         * 上传文件的字节数组
         * 文件的扩展名 jpg
         * 附加参数 如：拍摄地点 ：北京
         */
        //附加信息
        NameValuePair[] nameValuePairs = new NameValuePair[1];
        nameValuePairs[0] = new NameValuePair("author",fastDFSFile.getAuthor());

        /**
         * uploads[0] 文件上传所存储的storage的组的名称 group1
         * uploads[1] 文件存储到Storage上的名字      M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png
         */
        String[] uploads = storageClient.upload_file(fastDFSFile.getContent(), fastDFSFile.getExt(), null);
        return uploads;
    }


    /**
     *
     * 获取文件信息
     * @param groupName 文件的组名
     * @param remoteFileName    文件的存储路径名字
     * @throws Exception
     */
    public static FileInfo getFile(String groupName, String remoteFileName) throws Exception {
        //创建一个Tracker客户端的访问对象 TrackerClient
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient访问TrackerServer 获取连接
        TrackerServer connection = trackerClient.getConnection();
        //通过TrackerServer的连接信息可以获取storage的连接信息
        StorageClient storageClient = new StorageClient(connection, null);
        return storageClient.get_file_info(groupName,remoteFileName);
    }


    /**
     * M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png
     * 下载文件
     * @param groupName 文件的组名   M00
     * @param remoteFileName    文件的存储路径名字       00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png
     * @return
     * @throws Exception
     */
    public static InputStream downloadFile(String groupName, String remoteFileName) throws Exception {
        //创建一个Tracker客户端的访问对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient访问TrackerServer 获取连接
        TrackerServer connection = trackerClient.getConnection();
        //通过TrackerServer的连接信息可以获取stroage的连接信息
        StorageClient storageClient = new StorageClient(connection, null);


        byte[] bytes = storageClient.download_file(groupName, remoteFileName);
        return new ByteArrayInputStream(bytes);
    }

    /**
     * 删除文件
     * @param groupName 文件的组名
     * @param remoteFileName    文件的存储路径名字
     * @throws Exception
     */
    public static void deleteFile(String groupName, String remoteFileName) throws Exception {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer connection = trackerClient.getConnection();
        StorageClient storageClient = new StorageClient(connection, null);

        storageClient.delete_file(groupName,remoteFileName);
    }


    /**
     *  获取storage信息
     * @return
     * @throws IOException
     */
    public static StorageServer getStorages() throws IOException {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer connection = trackerClient.getConnection();
        return trackerClient.getStoreStorage(connection);
    }

    public static void main(String[] args) throws Exception {
//        FileInfo fileInfo = getFile("group1", "M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png");
//
//        System.out.println(fileInfo.getFileSize());
//        System.out.println(fileInfo.getCreateTimestamp());
//        System.out.println(fileInfo.getSourceIpAddr());

//        InputStream in = downloadFile("group1", "M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png");
//
//        FileOutputStream os = new FileOutputStream("D:/1.png");
//        int len;
//        byte[] bytes = new byte[1024];
//        while ((len=in.read(bytes))!=-1){
//            os.write(bytes,0,len);
//        }
//
//        in.close();
//        os.close();

//        deleteFile("group1", "M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png");

        StorageServer storages = getStorages();
        System.out.println(storages.getStorePathIndex());
    }
}
