package com.changgou.file.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.file.pojo.FastDFSFile;
import com.changgou.file.util.FastDFSClient;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 戴金华
 * @date 2019-12-05 15:52
 */
@RestController
@RequestMapping("/file")
@CrossOrigin
public class FileUploadController {


    @PostMapping("/upload")
    public Result upload(@RequestParam("file")MultipartFile file) throws Exception{
        FastDFSFile fastDFSFile = new FastDFSFile(
                file.getOriginalFilename(),
                file.getBytes(),
                StringUtils.getFilenameExtension(file.getOriginalFilename())
                );
        String[] upload = FastDFSClient.upload(fastDFSFile);

        //返回地址拼接 http://192.168.139.128:8080/group1/M00/00/00/wKiLgF3o37uAexh7AABOPvj9QjY616.png
        String url = "http://192.168.139.128:8080/"+upload[0]+"/"+upload[1];
        return new Result(true, StatusCode.OK,"上传成功",url);
    }


}
