package com.changgou.page.service;

/**
 * @author 戴金华
 * @date 2019-12-22 21:16
 */
public interface PageService {

    void generateItemPage(String skuId);
}
