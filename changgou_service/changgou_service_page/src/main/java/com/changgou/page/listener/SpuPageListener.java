package com.changgou.page.listener;

import com.changgou.page.service.PageService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 监听是否有商品上架
 * @author 戴金华
 * @date 2019-12-22 21:07
 */
@Component
public class SpuPageListener {

    @Autowired
    private PageService pageService;

    /**
     * 如果有商品上架 则生成静态页面
     */
    @RabbitListener(queues = {"page_create_queue"})
    public void createPage(String spuId){

        System.out.println("开始生成静态页面,spu的id是"+spuId);
        //生成静态页面
        pageService.generateItemPage(spuId);
    }
}
