package com.changgou.goods.service.impl;

import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-02 15:24
 */
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public List<Brand> findAll() {
        return brandMapper.selectAll();
    }

    @Override
    public Brand findById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public void add(Brand brand) {
        brandMapper.insert(brand);
    }

    @Override
    public void update(Brand brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public void delete(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Brand> findList(Map<String, Object> searchMap) {
        return brandMapper.selectByExample(getExample(searchMap));
    }

    @Override
    public Page<Brand> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        return (Page<Brand>) brandMapper.selectAll();
    }

    @Override
    public Page<Brand> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        return (Page<Brand>) brandMapper.selectByExample(getExample(searchMap));
    }

    @Override
    public List<Map> findListByCategoryName(String categoryName) {
        List<Map> map =  brandMapper.findListByCategoryName(categoryName);
        return map;
    }

    /**
     * 根据商品分类名称查询规格列表
     * @param categoryName
     * @return
     */
    @Override
    public List<Map> findSpecListByCategoryName(String categoryName) {
        return brandMapper.findSpecListByCategoryName(categoryName);
    }


    //构建查询条件
    private Example getExample(Map<String,Object> searchMap){
        Example example = new Example(Brand.class);
        Example.Criteria criteria = example.createCriteria();
        if (searchMap!=null){
            //如果查询条件不为空
            if (!"".equals(searchMap.get("name"))&&searchMap.get("name")!=null){
                //如果name参数不为空
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            if (!"".equals(searchMap.get("letter"))&&searchMap.get("letter")!=null){
                //如果name参数不为空
                criteria.andEqualTo("letter",searchMap.get("letter"));
            }

        }
        return example;
    }
}
