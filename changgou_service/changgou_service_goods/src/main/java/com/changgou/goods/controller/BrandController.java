package com.changgou.goods.controller;

import com.changgou.entity.PageResult;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-02 15:25
 */
@RestController
@RequestMapping("/brand")
//解决跨域调用问题
@CrossOrigin
public class BrandController {

    @Autowired
    private BrandService brandService;


    @GetMapping
    public Result findAll(){
        List<Brand> brands = brandService.findAll();
        return new Result(true, StatusCode.OK,"查询成功",brands);
    }

    @GetMapping("/{id}")
    public Result findById(@PathVariable Integer id){
        Brand brand = brandService.findById(id);
        return new Result(true,StatusCode.OK,"查询成功",brand);
    }

    @PostMapping
    public Result add(@RequestBody Brand brand){
        brandService.add(brand);
        return new Result(true,StatusCode.OK,"添加成功");
    }

    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id,@RequestBody Brand brand){
        brand.setId(id);
        brandService.update(brand);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        brandService.delete(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }

    @PostMapping("/search")
    public Result findList(@RequestParam Map<String,Object> searchMap){
        List<Brand> brands = brandService.findList(searchMap);
        return new Result(true,StatusCode.OK,"查询成功",brands);
    }

    //简单分页查询
    @GetMapping("/{page}/{size}")
    public Result findPage(@PathVariable int page,@PathVariable int size){
        Page<Brand> pageList = brandService.findPage(page, size);
        PageResult pageResult = new PageResult(pageList.getTotal(),pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }

    //条件查询+分页查询
    @GetMapping("/search/{page}/{size}")
    public Result findPage(@RequestParam Map<String,Object> searchMap,@PathVariable int page,@PathVariable int size){
        Page<Brand> pageList = brandService.findPage(searchMap, page, size);
        PageResult pageResult = new PageResult(pageList.getTotal(),pageList.getResult());
        return new Result(true,StatusCode.OK,"查询成功",pageResult);
    }


    //根据分类名称查找品牌列表
    @GetMapping("/category/{categoryName}")
    public Result findListByCategoryName(@PathVariable("categoryName") String categoryName){
        System.out.println(categoryName);
        List<Map> map = brandService.findListByCategoryName(categoryName);
        return new Result(true,StatusCode.OK,"查询成功",map);
    }

    @GetMapping("/spec/{categoryName}")
    public Result findSpecListByCategoryName(@PathVariable("categoryName") String categoryName){
        List<Map> map = brandService.findSpecListByCategoryName(categoryName);
        return new Result(true,StatusCode.OK,"查询成功",map);
    }
}
