package com.changgou.goods.dao;

import com.changgou.goods.pojo.Brand;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-02 15:22
 */
public interface BrandMapper extends Mapper<Brand> {


    /**
     * 根据分类名称查找品牌列表
     * @param categoryName
     * @return
     */
    @Select("SELECT name,image FROM tb_brand WHERE id IN " +
            "(SELECT brand_id FROM tb_category_brand WHERE category_id IN " +
            "(SELECT id FROM tb_category WHERE name = #{categoryName}))ORDER BY seq")
    public List<Map> findListByCategoryName(@Param("categoryName") String categoryName);


    /**
     * 根据商品分类名称查询规格列表
     * @param categoryName
     * @return
     */
    @Select("SELECT name,options FROM tb_spec WHERE template_id " +
            "IN (SELECT template_id FROM tb_category WHERE NAME = #{categoryName})")
    public List<Map> findSpecListByCategoryName(@Param("categoryName") String categoryName);
}
