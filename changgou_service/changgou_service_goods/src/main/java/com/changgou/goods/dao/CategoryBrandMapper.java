package com.changgou.goods.dao;

import com.changgou.goods.pojo.CategoryBrand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author 戴金华
 * @date 2019-12-07 9:19
 */
public interface CategoryBrandMapper extends Mapper<CategoryBrand> {
}
