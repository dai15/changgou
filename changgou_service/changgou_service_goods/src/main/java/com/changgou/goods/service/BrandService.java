package com.changgou.goods.service;

import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-02 15:23
 */
public interface BrandService {

    /**
     * 查询所有
     * @return
     */
    public List<Brand> findAll();


    /**
     * 根据id查询品牌
     * @param id
     * @return
     */
    public Brand findById(Integer id);


    /**
     * 新增品牌
     * @param brand
     */
    public void add(Brand brand);


    /**
     * 修改品牌
     * @param brand
     */
    public void update(Brand brand);

    /**
     * 删除品牌
     * @param id
     */
    public void delete(Integer id);

    /**
     * 根据条件查询品牌
     * @param searchMap
     * @return
     */
    public List<Brand> findList(Map<String,Object> searchMap);


    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    public Page<Brand> findPage(int page, int size);


    public Page<Brand> findPage(Map<String,Object> searchMap,int page,int size);

    /**
     * 根据分类名称查找品牌列表
     * @param categoryName
     * @return
     */
    public List<Map> findListByCategoryName(String categoryName);

    /**
     * 根据商品分类名称查询规格列表
     * @param categoryName
     * @return
     */
    public List<Map> findSpecListByCategoryName(String categoryName);
}
