package com.changgou.business.listener;

import okhttp3.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author 戴金华
 * @date 2019-12-20 16:36
 */
@Component
public class AdMqListener {


    @RabbitListener(queues = "ad_update_queue")
    public void messageHandler(String position){

        System.out.println(position);
        String url = "http://192.168.139.128/ad_update?position="+position;

        OkHttpClient okHttpClient = new OkHttpClient();
        //创建请求对象
        Request request = new Request.Builder().url(url).build();
        //发送请求
        Call call = okHttpClient.newCall(request);

        //调用发送请求后的回调方法 获取成功或者失败的信息
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                System.out.println("请求发送失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                System.out.println("====发送成功"+response.message());
            }
        });
    }
}
