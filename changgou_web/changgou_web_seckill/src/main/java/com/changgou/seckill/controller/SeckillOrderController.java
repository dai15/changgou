package com.changgou.seckill.controller;

import com.changgou.entity.Result;
import com.changgou.seckill.feign.SeckillOrderFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 戴金华
 * @date 2019-12-30 21:53
 */
@Controller
@CrossOrigin
@RequestMapping("/wcseckillOrder")
public class SeckillOrderController {

    @Autowired
    private SeckillOrderFeign seckillOrderFeign;


    @RequestMapping("/add")
    @ResponseBody
    public Result add(String time,Long id){
        Result result = seckillOrderFeign.add(time, id);
        return result;
    }

}
