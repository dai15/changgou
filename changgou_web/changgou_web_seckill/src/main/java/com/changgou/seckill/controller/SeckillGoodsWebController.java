package com.changgou.seckill.controller;

import com.changgou.entity.DateUtil;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.seckill.feign.SeckillFeign;
import com.changgou.seckill.pojo.SeckillGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-29 8:55
 */
@Controller
@RequestMapping("/wseckill")
public class SeckillGoodsWebController {


    @Autowired
    private SeckillFeign seckillFeign;

    /**
     * 获取不同时间段的集合
     * @return
     */
    @GetMapping("/getTimeMenus")
    @ResponseBody
    public Result getTimeMenus(){
        List<Date> dateMenus = DateUtil.getDateMenus();
        return new Result(true, StatusCode.OK,"查询成功",dateMenus);
    }


    /**
     * 跳转到秒杀页面
     * @return
     */
    @RequestMapping("/toIndex")
    public String toIndex(){
        return "seckill-index";
    }


    /**
     * 根据时间段查询秒杀商品的集合
     * @param time
     * @return
     */
    @GetMapping("/searchList")
    @ResponseBody
    public Result<List<SeckillGoods>> searchList(String time){
        List<SeckillGoods> seckillGoods = seckillFeign.searchList(DateUtil.formatStr(DateUtil.dealDateFormat(time))).getData();
        return new Result<>(true,StatusCode.OK,"查询成功",seckillGoods);
    }
}
