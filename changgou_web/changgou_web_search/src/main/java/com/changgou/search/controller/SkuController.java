package com.changgou.search.controller;

import com.changgou.search.feign.SkuFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-08 16:01
 */
@Controller
@RequestMapping("/search")
public class SkuController {


    @Autowired
    private SkuFeign skuSearchFeign;


    /**
     * 实现搜索调用
     *
     * @param searchMap
     * @return
     */
    @GetMapping("/list")
    public String search(@RequestParam(required = false) Map<String, String> searchMap, Model model) {
        Map<String, Object> resultMap = (Map<String, Object>) skuSearchFeign.search(searchMap).getData();
        model.addAttribute("resultMap", resultMap);
        //请求参数回调
        model.addAttribute("searchMap", searchMap);
        //url拼接
        StringBuilder url = new StringBuilder("/search/list?");
        if (searchMap != null && searchMap.size() > 0) {
            for (String key : searchMap.keySet()) {
                url.append(key + "=" + searchMap.get(key) + "&");
            }
        }
        model.addAttribute("url",url.toString());
        return "search";
    }
}
