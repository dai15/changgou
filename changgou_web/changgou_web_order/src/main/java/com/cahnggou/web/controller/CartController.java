package com.cahnggou.web.controller;

import com.alibaba.fastjson.JSON;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.feign.CartFeign;
import com.changgou.order.pojo.OrderItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-17 17:07
 */
@Controller
@RequestMapping("/wcart")
public class CartController {

    @Autowired
    private CartFeign cartFeign;


    /**
     * 查询
     * @param model
     * @return
     */
    @GetMapping("/list")
    public String list(Model model){
        List<Map<String, Object>> cartList = cartFeign.list();
        model.addAttribute("map",cartList);

        Integer[] total = getTotal(cartList);
        model.addAttribute("totalNum",total[0]);
        model.addAttribute("totalMoney",total[1]);
        return "cart";
    }

    @GetMapping("/add")
    @ResponseBody
    public Result add(@RequestParam("id") String id,@RequestParam("num") Integer num){
        cartFeign.add(id,num);
        List<Map<String, Object>> cartList = cartFeign.list();
        Integer[] total = getTotal(cartList);
        HashMap map = new HashMap();
        map.put("cart",cartList);
        map.put("totalNum",total[0]);
        map.put("totalMoney",total[1]);
        return new Result(true, StatusCode.OK,"添加成功",map);
    }

    private static Integer[] getTotal(List<Map<String, Object>> cartList){
        Integer[] total = new Integer[2];
        Integer totalNum = 0;
        Integer totalMoney = 0;
        for (Map<String, Object> map : cartList) {
            boolean checked = (boolean) map.get("checked");
            if (checked){
                //如果该项被选中 将总数量和总价格添加进去
                ObjectMapper mapper = new ObjectMapper();
                OrderItem orderItem = mapper.convertValue(map.get("item"), OrderItem.class);
                totalMoney+=orderItem.getMoney();
                totalNum+=orderItem.getNum();
            }
        }
        total[0] = totalNum;
        total[1] = totalMoney;
        return total;
    }

    @PostMapping("/updateChecked")
    @ResponseBody
    public Result updateChecked(String skuId,boolean checked){
        List<Map<String, Object>> cartList = cartFeign.updateChecked(skuId, checked);
        Map map = new HashMap();
        Integer[] total = getTotal(cartList);
        map.put("cart",cartList);
        map.put("totalNum",total[0]);
        map.put("totalMoney",total[1]);
        return new Result(true,StatusCode.OK,"更新成功",map);
    }

    @PostMapping("/delCart")
    @ResponseBody
    public Result delCart(){
        cartFeign.delCart();
        List<Map<String, Object>> cartList = cartFeign.list();
        Map map = new HashMap();
        Integer[] total = getTotal(cartList);
        map.put("cart",cartList);
        map.put("totalNum",total[0]);
        map.put("totalMoney",total[1]);
        return new Result(true,StatusCode.OK,"删除成功",map);
    }
}
