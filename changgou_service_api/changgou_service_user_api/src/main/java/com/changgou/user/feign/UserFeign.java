package com.changgou.user.feign;

import com.changgou.entity.Result;
import com.changgou.user.pojo.Address;
import com.changgou.user.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-12 9:41
 */
@FeignClient(name = "user")
public interface UserFeign {

    /**
     * 获取用户信息
     * @param username
     * @return
     */
    @GetMapping("/user/{username}")
    User findUserInfo(@PathVariable("username") String username);


    /**
     * 添加用户的积分
     * @param point
     * @return
     */
    @PostMapping("/user/add/{point}")
    Result addPoint(@PathVariable("point") Integer point);
}
