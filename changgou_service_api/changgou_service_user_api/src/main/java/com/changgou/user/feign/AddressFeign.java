package com.changgou.user.feign;

import com.changgou.entity.Result;
import com.changgou.user.pojo.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-24 14:38
 */
@FeignClient(name = "user")
@RequestMapping("/address")
public interface AddressFeign {

    /**
     * 用户收件地址信息
     */
    @GetMapping("/list")
    Result<List<Address>> list();
}
