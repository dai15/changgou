package com.changgou.order.feign;

import com.changgou.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-17 17:00
 */
@FeignClient(name = "order")
@RequestMapping("cart")
public interface CartFeign {

    /**
     * 添加购物车
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/add")
    Result add(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num);


    /**
     * 查询购物车的数据
     * @return
     */
    @GetMapping("/list")
    List<Map<String,Object>> list();


    /**
     * 更新勾选状态
     * @param skuId
     * @param checked
     * @return
     */
    @PostMapping("/updateChecked")
    List<Map<String,Object>> updateChecked(@RequestParam("skuId") String skuId,@RequestParam("checked") boolean checked);


    /**
     *  删除选中的商品
     * @return
     */
    @PostMapping("delCart")
    Result delCart();
}
