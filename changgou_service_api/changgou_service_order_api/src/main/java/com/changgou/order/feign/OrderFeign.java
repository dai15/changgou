package com.changgou.order.feign;

import com.changgou.entity.Result;
import com.changgou.order.pojo.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author 戴金华
 * @date 2019-12-25 9:59
 */
@FeignClient(name = "order")
@RequestMapping("/order")
public interface OrderFeign {
    /***
     * 提交订单的 数据
     * @param order
     * @return
     */
    @PostMapping
    Result add(@RequestBody Order order);

    /***
     * 根据ID查询数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result findById(@PathVariable("id") String id);
}
