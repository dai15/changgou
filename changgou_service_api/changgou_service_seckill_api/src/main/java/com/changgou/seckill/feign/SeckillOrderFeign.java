package com.changgou.seckill.feign;

import com.changgou.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 戴金华
 * @date 2019-12-30 21:51
 */
@FeignClient(name = "seckill")
@RequestMapping("/seckillOrder")
public interface SeckillOrderFeign {


    /**
     * 实现秒杀下单
     * @param time
     * @param id
     * @return
     */
    @RequestMapping("/add")
    Result add(@RequestParam("time") String time, @RequestParam("id") Long id);
}
