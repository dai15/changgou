package com.changgou.seckill.feign;

import com.changgou.entity.Result;
import com.changgou.seckill.pojo.SeckillGoods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-29 10:28
 */
@FeignClient(name = "seckill")
@RequestMapping("/seckill")
public interface SeckillFeign {


    /**
     * 根据时间段查询秒杀商品的集合
     * @param time
     * @return
     */
    @GetMapping("/searchList")
    Result<List<SeckillGoods>> searchList(@RequestParam("time") String time);


}
