package com.changgou.goods.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-07 8:45
 */
public class Goods implements Serializable {

    private Spu spu;

    private List<Sku> skuList;

    public Spu getSpu() {
        return spu;
    }

    public void setSpu(Spu spu) {
        this.spu = spu;
    }

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    @Override
    public String toString() {
        return "Goods{" + "spu=" + spu + ", skuList=" + skuList + '}';
    }
}
