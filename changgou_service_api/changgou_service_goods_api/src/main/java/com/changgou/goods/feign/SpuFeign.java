package com.changgou.goods.feign;

import com.changgou.entity.Result;
import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 戴金华
 * @date 2019-12-17 15:38
 */
@FeignClient(name = "goods")
@RequestMapping("/spu")
public interface SpuFeign {


    /**
     * 根据spu的id查询spu
     * @param id
     * @return
     */
    @GetMapping("/findSpuById/{id}")
    Result<Spu> findSpuById(@PathVariable("id") String id);

    /**
     * 根据spuId查询goods
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Goods> findGoodsById(@PathVariable("id") String id);
}
