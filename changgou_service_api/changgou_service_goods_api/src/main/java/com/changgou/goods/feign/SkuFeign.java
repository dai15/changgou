package com.changgou.goods.feign;

import com.changgou.entity.Result;
import com.changgou.goods.pojo.Sku;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 戴金华
 * @date 2019-12-08 17:16
 */
//value指定调用的eureka服务名称
@FeignClient(name = "goods")
@RequestMapping("/sku")
public interface SkuFeign {

    /**
     * 查询所有的sku数据
     * @return
     */
    @GetMapping
    Result<List<Sku>> findAll();


    /**
     * 更具id查询sku
     */
    @GetMapping("/{id}")
    Result<Sku> findById(@PathVariable("id") String id);


    /**
     * 根据spuId查询sku列表
     * @param spuId
     * @return
     */
    @GetMapping("/spuId/{spuId}")
    Result<List<Sku>> findListBySpuId(@PathVariable("spuId") String spuId);


    /**
     * 更改库存
     * @param username
     * @return
     */
    @PostMapping("/decr/count")
    Result decrCount(@RequestParam("username") String username);

    /**
     * 支付失败 sku库存回滚
     * @param skuId
     * @param num
     * @return
     */
    @PostMapping("/rollback")
    Result rollBack(@RequestParam("id") String skuId,@RequestParam("num") Integer num);
}
