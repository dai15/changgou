package com.changgou.pay.feign;

import com.changgou.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-26 11:10
 */
@FeignClient(name = "pay")
@RequestMapping("/wxpay")
public interface PayFeign {


    /**
     * 生成二维码信息
     * @param parameterMap
     * @return
     */
    @GetMapping("/nativePay")
    Result nativePay(@RequestParam Map<String,String> parameterMap);


    /**
     * 关闭微信支付订单
     * @param orderId
     * @return
     */
    @PutMapping("/close/{orderId}")
    Result closeOrder(@PathVariable String orderId);


    /**
     * 支付状态查询
     * @param parameterMap
     * @return
     */
    @GetMapping("/queryOrder")
    Result queryOrder(@RequestParam Map<String,String> parameterMap);
}
