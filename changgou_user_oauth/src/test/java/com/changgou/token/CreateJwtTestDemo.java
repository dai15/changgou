package com.changgou.token;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 戴金华
 * @date 2019-12-12 10:22
 * 令牌的创建和解析
 */
public class CreateJwtTestDemo {

    @Test
    public void testCreateToken(){
        //加载证书 读取类路径中的文件
        ClassPathResource resource = new ClassPathResource("changgou.jks");

        //读取证书数据
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, "changgou".toCharArray());

        //获取证书中的一对密钥
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("changgou", "changgou".toCharArray());

        //获取私钥->RSA算法
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();

        //创建令牌
        Map<String,Object> payload = new HashMap();
        payload.put("bucketName","hz-health-developer");
        payload.put("AK","sZPKCqgNu8Ec5ZYmFCiPxtF1SZ_8Zc0BCnQQW5V0");
        payload.put("SK","K4gcD0MfykafWs5uGjK4gkhxVRmp9Dyh_InlIvxq");

        //私钥作为签名生成算法中的盐
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(payload), new RsaSigner(rsaPrivateKey));

        //获取令牌数据
        String token = jwt.getEncoded();
        System.out.println(token);
    }


    /**
     * 令牌的解析(基于公钥进行解密)
     */
    @Test
    public void testParseToken(){
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjpudWxsLCJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTU3NzE1NTMzNywiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiJlYzFhNTc4Ny04NzdhLTQ2NmQtODk0Mi1hZWI2ODBlMmI3NmIiLCJjbGllbnRfaWQiOiJjaGFuZ2dvdSIsInVzZXJuYW1lIjoiaGVpbWEifQ.FrgUtcOL7tO8MHt2DQgSjw_q3MXep_QB7_WjXEl6DDG4V37aOwXOJFnAWZ6eU4JAu2i_aqbx2hTri8w3YM9bwFcm9iuwNgWB331DEsL9AgBw_rI9WDY8EgbnM6Y0bCHvUXwL1QsCSFK3C-uRymJa8jzcjJE6gi7X4PuFCQJZwkeIlzeZdMDCcHl9nihW1j1XltQ_psnvGFQhpaCPx-uJxJU8ha0RwMjPjsLrOLziKA9erhi3X-qpqX5UnMposi3rpB24QCfQ3c4qCox-tPk_lntzzrVXg0hsaJmA1OET9fTOVWCqNXz4YjS5No5N075OXyYdtYDNMhJ0YPqJiJzijw";
        String publicKey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjgM8AXx70VOGvZD6XfG/zb8ocvwLVp7QB+ejYRhrVQfV/R86ddfyLOkJpHaRhxEssuQhe+D01kiDd2S2NmjJmuLSA3kYoMJORv7nqrJ7Kw1pnv9O6UFQ769TtGLfHXBQz/ISybZSA9TPktUYOt3jSXO0cUUb12aQUprjuGu3ex9Iu6RmtPIBwS9c08NOOAXzhXJ7PsGraiU83xC/WufuKGkG4xF+ucBbbrXIQQl7k6YEmxUwzHSefN4o1zv3rGlcUVOEqmKZdOk3dgg7zb9x969SrSHBYvU4v37oc01ueF1yHW6I/ax38NAndbGfPOPH5OjS0G3PEOuLDgcvEfTsFQIDAQAB-----END PUBLIC KEY-----";
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));

        System.out.println(jwt.getClaims());
    }
}
