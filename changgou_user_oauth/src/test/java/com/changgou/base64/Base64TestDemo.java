package com.changgou.base64;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author 戴金华
 * @date 2019-12-12 16:26
 */
public class Base64TestDemo {


    @Test
    public void testParse(){
        String url = "Y2hhbmdnb3U6Y2hhbmdnb3U=";
        byte[] urlbytes = Base64.getDecoder().decode(url);
        String s = null;
        try {
            s = new String(urlbytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
