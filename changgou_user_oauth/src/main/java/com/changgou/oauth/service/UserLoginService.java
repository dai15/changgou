package com.changgou.oauth.service;

import com.changgou.oauth.util.AuthToken;

/**
 * @author 戴金华
 * @date 2019-12-12 11:08
 */
public interface UserLoginService {

    /**
     * 用户登录验证
     * @param username
     * @param password
     * @param clientId
     * @param clientSecret
     * @param grant_type
     */
    AuthToken login(String username, String password, String clientId, String clientSecret, String grant_type) throws Exception;
}
