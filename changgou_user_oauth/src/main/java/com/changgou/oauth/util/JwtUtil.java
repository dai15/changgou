package com.changgou.oauth.util;

import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

/**
 * @author 戴金华
 * @date 2019-12-24 11:15
 */
public class JwtUtil {



    public static String parseToken(String token){
        String publicKey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjgM8AXx70VOGvZD6XfG/zb8ocvwLVp7QB+ejYRhrVQfV/R86ddfyLOkJpHaRhxEssuQhe+D01kiDd2S2NmjJmuLSA3kYoMJORv7nqrJ7Kw1pnv9O6UFQ769TtGLfHXBQz/ISybZSA9TPktUYOt3jSXO0cUUb12aQUprjuGu3ex9Iu6RmtPIBwS9c08NOOAXzhXJ7PsGraiU83xC/WufuKGkG4xF+ucBbbrXIQQl7k6YEmxUwzHSefN4o1zv3rGlcUVOEqmKZdOk3dgg7zb9x969SrSHBYvU4v37oc01ueF1yHW6I/ax38NAndbGfPOPH5OjS0G3PEOuLDgcvEfTsFQIDAQAB-----END PUBLIC KEY-----";
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));

        return jwt.getClaims();
    }
}
