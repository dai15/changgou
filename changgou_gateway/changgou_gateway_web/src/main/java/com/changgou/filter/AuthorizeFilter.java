package com.changgou.filter;

//import com.changgou.util.JwtUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author 戴金华
 * @date 2019-12-11 22:08
 * 全局过滤
 * 实现用户权限 鉴别 校验
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {


    @Autowired
    private RedisTemplate redisTemplate;

    //令牌的名字
    private static final String AUTHORIZE_TOKEN="Authorization";
    /**
     * 全局拦截
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1. 获取请求
        ServerHttpRequest request = exchange.getRequest();


        //2. 获取响应
        ServerHttpResponse response = exchange.getResponse();
        System.out.println(request.getURI().getPath());
        //3. 如果是登录请求则放行
        if (request.getURI().getPath().contains("/user/toLogin")){
            return chain.filter(exchange);
        }
        //4. 获取请求头
        HttpHeaders headers = request.getHeaders();
        //5. 请求头中
        String token = headers.getFirst(AUTHORIZE_TOKEN);

        //boolean true:令牌在头文件中 false：令牌不在头文件中->将令牌分装到头文件中 再传递给其他微服务
        boolean hashToken = true;

        //6.判断请求头中是否有令牌
        if (StringUtils.isEmpty(token)){
            //如果请求头中 没有token信息  则尝试从请求参数中获取令牌信息
            token = request.getQueryParams().getFirst(AUTHORIZE_TOKEN);
            hashToken = false;
        }

        if (StringUtils.isEmpty(token)){
            //如果请求参数中 没有令牌的信息 尝试从cookie中获取
            HttpCookie cookie = request.getCookies().getFirst(AUTHORIZE_TOKEN);
            if (cookie!=null){
                token = cookie.getValue();
            }
        }

        if (StringUtils.isEmpty(token)){
            //如果cookie中 没有令牌信息 尝试从redis中获取
            token = (String) redisTemplate.boundValueOps(AUTHORIZE_TOKEN).get();
        }


        if (StringUtils.isEmpty(token)){
            String urlStr = request.getURI().getPath();
            try {
                urlStr = URLEncoder.encode(urlStr,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url = "http://localhost:9200/user/toLogin?From="+urlStr;
            //如果cookie中也没有令牌信息
            response.setStatusCode(HttpStatus.SEE_OTHER);
            response.getHeaders().set("Location",url);
            //跳转到认证服务
            return response.setComplete();
        }

        //9. 如果请有令牌 则解析
//        try {
//            JwtUtil.parseToken(token);
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            //10. 解析jwt令牌出错 说明令牌过期或者伪造等不合法情况出现
//            response.setStatusCode(HttpStatus.UNAUTHORIZED);
//            //11. 响应空数据
//            return response.setComplete();
//        }

        if (!hashToken){
            //如果令牌存在 且不在头文件中 将令牌放置再头文件中
            request.mutate().header(AUTHORIZE_TOKEN,token);
        }

        //12. 放行
        return chain.filter(exchange);
    }

    /**
     * 排序
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
