package cn.webdai.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.util.Date;

/**
 * @author 戴金华
 * @date 2019-12-11 16:55
 */
public class JwtTest {


    /**
     * 构建一个jwt令牌
     */
    @Test
    public void createJwt(){
        Date date = new Date(System.currentTimeMillis() + 1000 * 60);
        JwtBuilder jwtBuilder = Jwts.builder()
                .setId("888")       //设置唯一编号
                .setSubject("小白")   //设置主题  可以是json数据
                .setIssuedAt(new Date())    //设置签发日期
                .setExpiration(date)        //设置过期时间 参数为date类型
                .signWith(SignatureAlgorithm.HS256, "daijinhua");   //设置签名 使用HS256
        System.out.println(jwtBuilder.compact());
    }

    /**
     * 解析一个jwt
     */
    @Test
    public void parseToken(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NzYwNTQ3ODF9.I1Vh1B4PFvt1ykp9nEBSesPptMLCmduYbARSL8IkQKY";
        Claims claims = Jwts.parser().setSigningKey("daijinhua").parseClaimsJws(token).getBody();
        System.out.println(claims);
    }
}
